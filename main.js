//je définis ma variable nombre d'essais
let tries;
// je définis ma variable nombre aléatoire
let randomNumber;

//je définis ma variable meilleur score et enregistre le meilleur score dans le localstorage
let bestScore = localStorage.getItem("bestScore") || "non défini";
const bestScoreElement = document.getElementById("bestScore");
bestScoreElement.innerHTML = "Le meilleur score est " + bestScore;

// je crée la fonction qui permet de définir un nombre aléatoire entre 1 et 100
window.addEventListener("DOMContentLoaded", function (event) {
  function startGame() {
    tries = 1;
    randomNumber = Math.floor(Math.random() * 100);
    console.log("le nombre à deviner est :" + randomNumber);
  }
  startGame();
});

// je récupère le formulaire
const form = document.getElementById("guess_form");

// je crée de nouvelles variables me permettant de rajouter des éléments dans le HTML
let newP = document.createElement("p");
let conteneur = document.getElementById("conteneur");
conteneur.appendChild(newP);

// je crée récupère la valeur du formulaire et je la compare au nombre aléatoire et je rajoute un paragraphe dans le Html en fonction de la situation
function submitForm(event) {
  event.preventDefault();
  console.log("soumission du formulaire");
  const value = form.guess.value;
  console.log(value);
  if (value > 100 || value < 1) {
    newP.textContent = "Cette valeur n'est pas permise!";
  } else if (value < randomNumber) {
    newP.textContent = "Le nombre à deviner est supérieur, essaye encore!";
    tries++;
  } else if (value > randomNumber) {
    newP.textContent = "Le nombre à deviner est inférieur, essaye encore";
    tries++;
  } else {
    newP.textContent = "Bravo, tu as deviné le nombre!";

    //je compare le nombre d'essai avec le meilleur score enregistré
    if (tries < bestScore || bestScore === "non défini") {
      bestScore = tries;
      localStorage.setItem("bestScore", bestScore);

      // je mets à jour l'affichage du meilleur score sur la page si nécessaire
      bestScoreElement.innerHTML = "Le meilleur score est " + bestScore;
    }
  }
  console.log(tries);
}

// la fonction submitform s'exécute à l'évenement click submit
form.addEventListener("submit", submitForm);
